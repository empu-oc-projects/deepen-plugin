<?php namespace Empu\Deepen\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Illuminate\Support\Carbon;

/**
 * MonthSelect Form Widget
 */
class MonthSelect extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'empu_deepen_month_select';

    public $monthPlaceholder = 'Select month...';

    public $minYear = null;

    public $maxYear = null;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig([
            'monthPlaceholder',
            'minYear',
            'maxYear',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('monthselect');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
        $this->vars['monthOpts'] = collect(range(1, 12))->mapWithKeys(function ($m) {
            return [$m => Carbon::createFromDate(null, $m)->localeMonth];
        });
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/monthselect.css', 'Empu.Deepen');
        // $this->addJs('js/monthselect.js', 'Empu.Deepen');
    }

    public function getLoadValue()
    {
        $monthValue = parent::getLoadValue();
        $month = null;
        $year = null;

        if ($monthValue instanceof \DateTime) {
            $month = $monthValue->format('m');
            $year = $monthValue->format('Y');
        }
        elseif (preg_match('~^(\d{4})\-(\d{1,2})(\-(\d{1,2})\.*)?~', $monthValue, $matches))
        {
            $month = $matches[2];
            $year = $matches[1];
        }

        return ['_month' => $month, '_year' => $year];
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        $year = empty($value['_year']) ? null : $value['_year'];
        $month = empty($value['_month']) ? null : $value['_month'];

        return ($year || $month) ? Carbon::createFromDate($year, $month) : null;
    }
}
