<?php namespace Empu\Deepen\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * Plain text form widget
 */
class PlainText extends FormWidgetBase
{
    public $cssClass = '';

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'empu_deepen_plaintext';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig(['cssClass']);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->vars['value'] = $this->getLoadValue();

        return $this->makePartial('control');
    }
}
