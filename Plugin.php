<?php namespace Empu\Deepen;

use Backend;
use Empu\Deepen\Components\Deepen;
use Empu\Deepen\Facades\BsForm;
use Empu\Deepen\Libs\BootstrapForm;
use October\Rain\Html\FormBuilder;
use October\Rain\Support\Facades\Form;
use System\Classes\MarkupManager;
use System\Classes\PluginBase;

/**
 * Deepen Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Deepen',
            'description' => 'Enhance your october',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-level-up'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('bsform', function($app) {
            return new BootstrapForm($app['html'], $app['form']);
        });
        
        MarkupManager::instance()->registerCallback(function ($manager) {
            $manager->registerFunctions([
                'bsform_*' => [BsForm::class, '*'],
            ]);
        });

        FormBuilder::macro('date', function($name, $value = null, $options = []) {
            return Form::input('date', $name, $value, $options);
        });
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            Deepen::class => 'deepen',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'empu.deepen.some_permission' => [
                'tab' => 'Deepen',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'deepen' => [
                'label'       => 'Deepen',
                'url'         => Backend::url('empu/deepen/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.deepen.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerFormWidgets()
    {
        return [
            FormWidgets\MonthSelect::class => 'monthselect',
            FormWidgets\PlainText::class => 'static',
            FormWidgets\StromClearfix::class => 'stromclearfix',
        ];
    }
    
    public function registerMarkupTags()
    {
        // return [
        //     'functions' => [
        //         'bsform_*' => [BsForm::class, '*'],
        //     ]
        // ];
    }
}
