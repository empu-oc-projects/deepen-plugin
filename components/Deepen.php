<?php namespace Empu\Deepen\Components;

use Cms\Classes\ComponentBase;

class Deepen extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Deepen Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->addJs('/plugins/empu/deepen/assets/js/clamp.js');
        $this->addJs('/plugins/empu/deepen/assets/js/deepen.js');
    }
}
