(function ($) {
  
  $.fn.clamp = function (options) {

    var styles = {
      display: 'block',
      overflow: 'hidden'
    };

    this.each(function () {
      var el = $(this);
      var target = el.attr('href') || el.data('target');
      var targetEl = $(target);
      var height = targetEl.data('clamp') || '200px';

      targetEl.css(styles);

      if (! targetEl.hasClass('expand')) {
        targetEl.css({ height: height });
      }

      el.click(function (ev) {
        var invText = el.data('inverseText');

        if (el.hasClass('clamped')) {
          targetEl.css({ height: height });
        }
        else {
          targetEl.css({ height: 'auto' });
        }

        el.toggleClass('clamped')
          .data('inverseText', el.text())
          .text(invText);
      });

    });


    return this;
  }

}(jQuery));