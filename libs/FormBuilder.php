<?php

namespace Empu\Deepen\Libs;

class FormBuilder
{
    public static function group($input, $label = null, array $options = [])
    {
        $options = array_merge(
            [
                'wrapperClassName' => 'form-group',
                'required' => false,
            ],
            $options
        );

        extract($options, EXTR_PREFIX_ALL, 'opt');

        // return 'asdf';
        return (
            '<div class="'.$opt_wrapperClassName.'">'
                .$label
                .($opt_required ? '<span class="label-required-flag">*</span>' : '')
                .$input
                .($opt_name ? '<small class="invalid-feedback" data-validate-for="'.$opt_name.'"></small>' : '')
            .'</div>'
        );
    }
}
