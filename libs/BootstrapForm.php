<?php

namespace Empu\Deepen\Libs;

use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\Lang;
use October\Rain\Html\FormBuilder;
use October\Rain\Html\HtmlBuilder;

class BootstrapForm
{
    /**
     * Vertical form class.
     *
     * @var string
     */
    const LAYOUT_VERTICAL = 'vertical';

    /**
     * Horizontal form class.
     *
     * @var string
     */
    const LAYOUT_HORIZONTAL = 'horizontal';

    /**
     * Inline form class.
     *
     * @var string
     */
    const LAYOUT_INLINE = 'inline';

    /**
     * October HtmlBuilder instance.
     *
     * @var \October\Rain\Html\HtmlBuilder
     */
    protected $html;

    /**
     * October FormBuilder instance.
     *
     * @var \October\Rain\Html\FormBuilder
     */
    protected $form;

    /**
     * Bootstrap form layout.
     *
     * @var string
     */
    protected $layout;

    /**
     * Bootstrap form left column class.
     *
     * @var string
     */
    protected $leftColumnClass;

    /**
     * Bootstrap form right column class.
     *
     * @var string
     */
    protected $rightColumnClass;


    /**
     * Construct the class.
     *
     * @param  \October\Rain\Html\HtmlBuilder             $html
     * @param  \October\Rain\Html\FormBuilder             $form
     * @return void
     */
    public function __construct(HtmlBuilder $html, FormBuilder $form)
    {
        $this->html = $html;
        $this->form = $form;
    }

    /**
     * Open a form while passing a model and the routes for storing or updating
     * the model. This will set the correct route along with the correct
     * method.
     *
     * @param  array  $options
     * @return string
     */
    public function open(array $options = [])
    {
        return $this->form->open($options);
    }

    public function setLayout(string $layout, array $options = [])
    {
        $validLayouts = [self::LAYOUT_HORIZONTAL, self::LAYOUT_VERTICAL, self::LAYOUT_INLINE];

        if (! in_array($layout, $validLayouts)) {
            $layout = self::LAYOUT_HORIZONTAL;
        }

        $this->layout = $layout;

        if (array_key_exists('left_column_class', $options)) {
            $this->setLeftColumnClass($options['left_column_class']);
        }
        if (array_key_exists('right_column_class', $options)) {
            $this->setRightColumnClass($options['right_column_class']);
        }

        return '<!-- form -->';
    }

    /**
     * Reset and close the form.
     *
     * @return string
     */
    public function close()
    {
        $this->layout = null;

        $this->leftColumnClass = $this->rightColumnClass = null;

        return $this->form->close();
    }

    public function group($element, string $name = null, string $label = null, array $options = []) 
    {
        if ($this->isHorizontal()) {
            return $this->horizontalGroup($element, $name, $label, $options);
        }
        else {
            return $this->verticalGroup($element, $name, $label, $options);
        }
    }

    public function horizontalGroup($element, string $name = null, string $label = null, array $options = [])
    {
        $inputWrapperClass = $this->getRightColumnClass();
        $labelElement = empty($label) ? '' : $this->label($name, $label, $options);

        if (is_array($element)) {
            $inputWrapperClass .= ' input-group';
            $element = implode('', $element);
        }
        
        return (
            '<div class="form-group row">' .
                $labelElement .
                '<div class="'.$inputWrapperClass.'">' .
                    $element .
                    (! empty($name) ? ($this->getFieldFeedback($name, false) . $this->getHelpText($name/* , $optionsField */)) : '') .
                '</div>' .
            '</div>'
        );
    }

    public function verticalGroup($element, string $name = null, string $label = null, array $options = [])
    {
        $labelElement = empty($label) ? '' : $this->label($name, $label, $options);

        if (is_array($element)) {
            $element = '<div class="input-group">' . implode('', $element) . '</div>';
        }

        return (
            '<div class="form-group">' .
                $labelElement .
                $element .
                (! empty($name) ? ($this->getFieldFeedback($name, false) . $this->getHelpText($name/* , $optionsField */)) : '') .
            '</div>'
        );
    }

    protected function getFieldFeedback(string $name, bool $isValid = true, string $message = null)
    {
        $attrs = [
            'class' => $isValid ? 'text-success' : 'text-danger',
            'data-validate-for' => $name,
        ];

        return '<small '.$this->html->attributes($attrs).'>'. $message .'</small>';
    }

    /**
     * Create a Bootstrap static field.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function staticField($name, $label = null, $value = null, array $options = [])
    {
        $options = array_merge(['class' => 'form-control-plaintext'], $options, ['readonly' => true]);

        if (is_array($value) and isset($value['html'])) {
            $value = $value['html'];
        } else {
            $value = e($value);
        }

        $label = $this->getLabelTitle($label, $name);
        $inputElement = $this->form->text($name, $value, $options);

        return $this->group($inputElement, $name, $label);
    }

    /**
     * Create a Bootstrap text field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function text($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('text', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap email field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function email($name = 'email', $label = null, $value = null, array $options = [])
    {
        return $this->input('email', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap URL field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function url($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('url', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap tel field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function tel($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('tel', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap number field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function number($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('number', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap date field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function date($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('date', $name, $label, $value, $options);
    }

     /**
     * Create a Bootstrap email time input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function time($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('time', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap textarea field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function textarea($name, $label = null, $value = null, array $options = [])
    {
        return $this->input('textarea', $name, $label, $value, $options);
    }

    /**
     * Create a Bootstrap password field input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $options
     * @return string
     */
    public function password($name = 'password', $label = null, array $options = [])
    {
        return $this->input('password', $name, $label, null, $options);
    }

    /**
     * Create a Bootstrap checkbox input.
     *
     * @param  string   $name
     * @param  string   $label
     * @param  string   $value
     * @param  bool     $checked
     * @param  array    $options
     * @return string
     */
    public function checkbox($name, $label = null, $value = 1, $checked = null, array $options = [])
    {
        $inputElement = $this->checkboxElement($name, $label, $value, $checked, $options);

        return $this->group($inputElement, $name, null);
    }

    /**
     * Create a single Bootstrap checkbox element.
     *
     * @param  string   $name
     * @param  string   $label
     * @param  string   $value
     * @param  bool     $checked
     * @param  bool     $inline
     * @param  array    $options
     * @return string
     */
    public function checkboxElement($name, $label = null, $value = 1, $checked = null, array $options = [])
    {
        $value = is_null($value) ? $label : $value;
        $options = array_merge(['class' => 'form-check-input'], $options);
        $inline = array_pull($options, 'inline', false);
        $inputElement = $this->form->checkbox($name, $value, $checked, $options);

        return $this->formCheck($inputElement, $name, $label, $inline);
    }

    /**
     * Create a collection of Bootstrap checkboxes.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $choices
     * @param  array   $checkedValues
     * @param  bool    $inline
     * @param  array   $options
     * @return string
     */
    public function checkboxes($name, $label = null, $choices = [], $checkedValues = [], array $options = [])
    {
        $elements = '';

        foreach ($choices as $value => $choiceLabel) {
            $checked = in_array($value, (array) $checkedValues);

            $elements .= $this->checkboxElement($name, $choiceLabel, $value, $checked, $options);
        }

        return $this->group($elements, $name, $label);
    }

    public function checkboxSet($name, $choices = [], array $checkedValue = [], array $options = [], bool $join = true)
    {
        $elements = [];

        foreach ($choices as $value => $choiceLabel) {
            $checked = in_array($value, $checkedValue);

            array_push($elements, $this->checkboxElement($name, $choiceLabel, $value, $checked, $options));
        }

        return $join ? implode('', $elements) : $elements;
    }

    /**
     * Create a Bootstrap radio input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  bool    $checked
     * @param  array   $options
     * @return string
     */
    public function radio($name, $label = null, $value = null, $checked = null, array $options = [])
    {
        $inputElement = $this->radioElement($name, $label, $value, $checked, $options);

        return $this->group($inputElement, $name, null);
    }

    /**
     * Create a single Bootstrap radio input.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  bool    $checked
     * @param  bool    $inline
     * @param  array   $options
     * @return string
     */
    public function radioElement($name, $label = null, $value = null, $checked = null, array $options = [])
    {
        $value = is_null($value) ? $label : $value;
        $options = array_merge(['class' => 'form-check-input'], $options);
        $inline = array_pull($options, 'inline', false);
        $inputElement = $this->form->radio($name, $value, $checked, $options);

        return $this->formCheck($inputElement, "{$name}-{$value}", $label, $inline);
    }

    /**
     * Create a collection of Bootstrap radio inputs.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $choices
     * @param  string  $checkedValue
     * @param  bool    $inline
     * @param  array   $options
     * @return string
     */
    public function radios($name, $label = null, $choices = [], $checkedValue = null, array $options = [])
    {
        $element = $this->radioSet($name, $choices, $checkedValue, $options);

        return $this->group($element, $name, $label);
    }

    public function radioSet($name, $choices = [], $checkedValue = null, array $options = [], bool $join = true)
    {
        $elements = [];

        foreach ($choices as $value => $choiceLabel) {
            $checked = $value === $checkedValue;

            array_push($elements, $this->radioElement($name, $choiceLabel, $value, $checked, $options));
        }

        return $join ? implode('', $elements) : $elements;
    }

    public function formCheck($input, $name, $label = null, $inline = false)
    {
        $label = $label === false ? null : $this->getLabelTitle($label, $name);
        $wrapperClass = $inline ? 'form-check form-check-inline' : 'form-check';

        return (
            '<div class="'.$wrapperClass.'">' .
                '<label class="form-check-label">' .
                $input .
                $label .
                '</label>' .
                // $this->form->label($name, $label, ['class' => 'form-check-label']) .
            '</div>'
        );
    }

    /**
     * Create a Bootstrap label.
     *
     * @param  string  $name
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function label($name, $value = null, array $options = [])
    {
        $options = $this->getLabelOptions($options);
        $escapeHtml = false;
        
        if (is_array($value) && isset($value['html'])) {
            $value = $value['html'];
        }
        elseif ($value instanceof HtmlString) {
            $value = $value->toHtml();
        }
        else {
            $escapeHtml = true;
        }

        if (array_pull($options, 'required', false)) {
            $value .= ' *';
        }

        return $this->form->label($name, $value, $options, $escapeHtml);
    }

    /**
     * Create a Boostrap submit button.
     *
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function submit($value = null, array $options = [])
    {
        $options = array_merge(['class' => 'btn btn-primary'], $options);
        $inputElement = $this->form->submit($value, $options);

        return $this->group($inputElement);
    }

    /**
     * Create a Boostrap button.
     *
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function button($value = null, array $options = [])
    {
        $options = array_merge(['class' => 'btn btn-primary'], $options);
        $inputElement = $this->form->button($value, $options);

        return $this->group($inputElement);
    }

    /**
     * Create a Boostrap file upload button.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $options
     * @return string
     */
    public function file($name, $label = null, array $options = [])
    {
        $label = $this->getLabelTitle($label, $name);

        $options = array_merge(['class' => 'filestyle', 'data-buttonBefore' => 'true'], $options);

        $options = $this->getFieldOptions($options, $name);
        $inputElement = $this->form->input('file', $name, null, $options);

        $wrapperOptions = $this->isHorizontal() ? ['class' => $this->getRightColumnClass()] : [];
        $wrapperElement = '<div' . $this->html->attributes($wrapperOptions) . '>' . $inputElement . $this->getFieldError($name) . $this->getHelpText($name, $options) . '</div>';

        return $this->getFormGroup($name, $label, $wrapperElement);
    }

    /**
     * Create the input group for an element with the correct classes for errors.
     *
     * @param  string  $type
     * @param  string  $name
     * @param  string  $label
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function input($type, $name, $label = null, $value = null, array $options = [])
    {
        $label = $this->getLabelTitle($label, $name);
        $optionsField = $this->getFieldOptions(array_except($options, ['suffix', 'prefix']), $name);

        if ($type === 'password') {
            $inputElements[] = $this->form->password($name, $optionsField);
        }
        elseif (in_array($type, ['date', 'time', 'tel', 'number'])) {
            $inputElements[] = $this->form->input($type, $name, $value, $optionsField);
        }
        else {
            $inputElements[] = $this->form->{$type}($name, $value, $optionsField);
        }

        if (isset($options['prefix']))
        {
            $prependElement = '<div class="input-group-prepend"><span class="input-group-text">' .
                $options['prefix'] . '</span></div>';

            array_unshift($inputElements, $prependElement);
        }

        if (isset($options['suffix'])) {
            $appendElement = '<div class="input-group-append"><span class="input-group-text">' .
                $options['suffix'] . '</span></div>';

            array_push($inputElements, $appendElement);
        }

        if (count($inputElements) > 1) {
            return $this->group($inputElements, $name, $label, $options);
        }
        else {
            return $this->group($inputElements[0], $name, $label, $options);
        }
    }

    /**
     * Create an addon button element.
     *
     * @param  string  $label
     * @param  array  $options
     * @return string
     */
    public function addonButton($label, $options = [])
    {
        $attributes = array_merge(['class' => 'btn', 'type' => 'button'], $options);

        if (isset($options['class'])) {
            $attributes['class'] .= ' btn';
        }

        return '<div class="input-group-btn"><button ' . $this->html->attributes($attributes) . '>'.$label.'</button></div>';
    }

    /**
     * Create an addon text element.
     *
     * @param  string  $text
     * @param  array  $options
     * @return string
     */
    public function addonText($text, $options = [])
    {
        return '<div class="input-group-addon"><span ' . $this->html->attributes($options) . '>'.$text.'</span></div>';
    }

    /**
     * Create an addon icon element.
     *
     * @param  string  $icon
     * @param  array  $options
     * @return string
     */
    public function addonIcon($icon, $options = [])
    {
        $prefix = array_get($options, 'prefix', $this->getIconPrefix());

        return '<div class="input-group-addon"><span ' . $this->html->attributes($options) . '><i class="'.$prefix.$icon.'"></i></span></div>';
    }

    /**
     * Create a hidden field.
     *
     * @param  string  $name
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function hidden($name, $value = null, $options = [])
    {
        return $this->form->hidden($name, $value, $options);
    }

    /**
     * Create a select box field.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  array   $list
     * @param  string  $selected
     * @param  array   $options
     * @return string
     */
    public function select($name, $label = null, $list = [], $selected = null, array $options = [])
    {
        $label = $this->getLabelTitle($label, $name);
        $options = $this->getFieldOptions($options, $name);
        $inputElement = isset($options['prefix']) ? $options['prefix'] : '';
        $inputElement .= $this->form->select($name, $list, $selected, $options);

        if (isset($options['suffix'])) {
            $inputElement .= $options['suffix'];
        }

        if (isset($options['prefix']) || isset($options['suffix'])) {
            $inputElement = '<div class="input-group">' . $inputElement . '</div>';
        }

        return $this->group($inputElement, $name, $label);
    }

    /**
     * Wrap the content in Laravel's HTML string class.
     *
     * @param  string  $html
     * @return \Illuminate\Support\HtmlString
     */
    protected function toHtmlString($html)
    {
        return new HtmlString($html);
    }

    /**
     * Get the label title for a form field, first by using the provided one
     * or titleizing the field name.
     *
     * @param  string  $label
     * @param  string  $name
     * @return mixed
     */
    protected function getLabelTitle($label, $name)
    {
        if ($label === false) {
            return null;
        }

        if (is_null($label) && Lang::has("forms.{$name}")) {
            return Lang::get("forms.{$name}");
        }

        return $label ?: str_replace('_', ' ', Str::title($name));
    }

    /**
     * Get a form group comprised of a form element and errors.
     *
     * @param  string  $name
     * @param  string  $element
     * @return \Illuminate\Support\HtmlString
     */
    protected function getFormGroupWithoutLabel($name, $element)
    {
        $options = $this->getFormGroupOptions($name);

        return $this->toHtmlString('<div' . $this->html->attributes($options) . '>' . $element . '</div>');
    }

    /**
     * Get a form group comprised of a label, form element and errors.
     *
     * @param  string  $name
     * @param  string  $value
     * @param  string  $element
     * @return \Illuminate\Support\HtmlString
     */
    protected function getFormGroupWithLabel($name, $value, $element)
    {
        $options = $this->getFormGroupOptions($name);

        return $this->toHtmlString('<div' . $this->html->attributes($options) . '>' . $this->label($name, $value) . $element . '</div>');
    }

    /**
     * Get a form group with or without a label.
     *
     * @param  string  $name
     * @param  string  $label
     * @param  string  $element
     * @return string
     */
    public function getFormGroup($name = null, $label = null, $wrapperElement)
    {
        if (is_null($label)) {
            return $this->getFormGroupWithoutLabel($name, $wrapperElement);
        }
        return $this->getFormGroupWithLabel($name, $label, $wrapperElement);
    }

    /**
     * Merge the options provided for a form group with the default options
     * required for Bootstrap styling.
     *
     * @param  string $name
     * @param  array  $options
     * @return array
     */
    protected function getFormGroupOptions($name = null, array $options = [])
    {
        $class = 'form-group';

        if ($name) {
            $class .= ' ' . $this->getFieldErrorClass($name);
        }

        return array_merge(['class' => $class], $options);
    }

    /**
     * Merge the options provided for a field with the default options
     * required for Bootstrap styling.
     *
     * @param  array  $options
     * @param  string $name
     * @return array
     */
    protected function getFieldOptions(array $options = [], $name = null)
    {
        $options['class'] = trim('form-control ' . $this->getFieldOptionsClass($options));

        // If we've been provided the input name and the ID has not been set in the options,
        // we'll use the name as the ID to hook it up with the label.
        if ($name && ! array_key_exists('id', $options)) {
            $options['id'] = $name;
        }

        return $options;
    }

    /**
     * Returns the class property from the options, or the empty string
     *
     * @param   array  $options
     * @return  string
     */
    protected function getFieldOptionsClass(array $options = [])
    {
        return array_get($options, 'class');
    }

    /**
     * Merge the options provided for a label with the default options
     * required for Bootstrap styling.
     *
     * @param  array  $options
     * @return array
     */
    protected function getLabelOptions(array $options = [])
    {
        $class = [];

        if ($this->isHorizontal()) {
            array_push($class, 'col-form-label', $this->getLeftColumnClass());
        }

        if (array_get($options, 'required', false)) {
            array_push($class, 'is-required');
        }

        return array_merge(['class' => join(' ', $class)], $options);
    }

    /**
     * Get the form type.
     *
     * @return string
     */
    public function getLayout()
    {
        return isset($this->layout) ? $this->layout : self::LAYOUT_VERTICAL;
    }

    /**
     * Determine if the form is of a horizontal type.
     *
     * @return bool
     */
    public function isHorizontal()
    {
        return $this->getLayout() === self::LAYOUT_HORIZONTAL;
    }

    /**
     * Get the column class for the left column of a horizontal form.
     *
     * @return string
     */
    public function getLeftColumnClass()
    {
        return $this->leftColumnClass ?: 'col-sm-3';
    }

    /**
     * Set the column class for the left column of a horizontal form.
     *
     * @param  string  $class
     * @return void
     */
    public function setLeftColumnClass($class)
    {
        $this->leftColumnClass = $class;
    }

    /**
     * Get the column class for the right column of a horizontal form.
     *
     * @return string
     */
    public function getRightColumnClass()
    {
        return $this->rightColumnClass ?: 'col-sm-9';
    }

    /**
     * Set the column class for the right column of a horizontal form.
     *
     * @param  string  $class
     * @return void
     */
    public function setRightColumnClass($class)
    {
        $this->rightColumnClass = $class;
    }

    /**
     * Get the icon prefix.
     *
     * @return string
     */
    public function getIconPrefix()
    {
        return $this->iconPrefix ?: $this->config->get('bootstrap_form.icon_prefix');
    }

     /**
     * Get the error class.
     *
     * @return string
     */
    public function getErrorClass()
    {
        return $this->errorClass ?: $this->config->get('bootstrap_form.error_class');
    }

    /**
     * Get the error bag.
     *
     * @return string
     */
    protected function getErrorBag()
    {
        return $this->errorBag ?: $this->config->get('bootstrap_form.error_bag');
    }

    /**
     * Set the error bag.
     *
     * @param  $errorBag  string
     * @return void
     */
    protected function setErrorBag($errorBag)
    {
        $this->errorBag = $errorBag;
    }

    /**
     * Flatten arrayed field names to work with the validator, including removing "[]",
     * and converting nested arrays like "foo[bar][baz]" to "foo.bar.baz".
     *
     * @param  string  $field
     * @return string
     */
    public function flattenFieldName($field)
    {
        return preg_replace_callback("/\[(.*)\\]/U", function ($matches) {
            if (!empty($matches[1]) || $matches[1] === '0') {
                return "." . $matches[1];
            }
        }, $field);
    }

    /**
     * Get the MessageBag of errors that is populated by the
     * validator.
     *
     * @return \Illuminate\Support\MessageBag
     */
    protected function getErrors()
    {
        return $this->form->getSessionStore()->get('errors');
    }

    /**
     * Get the first error for a given field, using the provided
     * format, defaulting to the normal Bootstrap 3 format.
     *
     * @param  string  $field
     * @param  string  $format
     * @return mixed
     */
    protected function getFieldError($field, $format = '<span class="help-block">:message</span>')
    {
        $field = $this->flattenFieldName($field);

        if ($this->getErrors()) {
            $allErrors = $this->config->get('bootstrap_form.show_all_errors');

            if ($this->getErrorBag()) {
                $errorBag = $this->getErrors()->{$this->getErrorBag()};
            } else {
                $errorBag = $this->getErrors();
            }

            if ($allErrors) {
                return implode('', $errorBag->get($field, $format));
            }

            return $errorBag->first($field, $format);
        }
    }

    /**
     * Return the error class if the given field has associated
     * errors, defaulting to the normal Bootstrap 3 error class.
     *
     * @param  string  $field
     * @param  string  $class
     * @return string
     */
    protected function getFieldErrorClass($field)
    {
        return $this->getFieldError($field) ? $this->getErrorClass() : null;
    }

    /**
     * Get the help text for the given field.
     *
     * @param  string  $field
     * @param  array   $options
     * @return \Illuminate\Support\HtmlString
     */
    protected function getHelpText($field, array $options = [])
    {
        if (array_key_exists('help_text', $options)) {
            return $this->toHtmlString('<span class="help-block">' . e($options['help_text']) . '</span>');
        }

        return '';
    }
}
